# Backend developer coding tasks (8 hours)

### Brief
Build the API for a game catalog. The game has players who play it very frequently. The games come in versions so players can own a game in one or more versions of the game.

The system should only store one game play record per player per game per day even if the player played the game(s) multiple times in a day provided they played it alone. If the player played with other people, the system records who started the game and those invited to join.

> Assumption 1: Each of game only allows a maximum of 4 players.
> Assumption 2: Players can only play together if they have the same game versions.

### Setup a database and create test data like below:
1. For players, store their name, email, nickname, password, date joined, last login
2. For games, store their name, version, date added

### You should have APIs that can do the following:
1. Return all the games
2. Return all the players, their games and their gameplays (overall and for each game)
3. Return all the games played per day and their players
4. Return all the games played within a date range
5. Return the top 100 players month by month with a link to see the games they played

### Write a script to generate data with the following requirements:
1. 3,835 days of gaming.
2. There are 10,000 players in the system.
3. 5 games (Call of Duty, Mortal Kombat, FIFA, Just Cause, Apex Legend) with different versions from 2010 to 2020.
4. There are at least 1,500 gameplays every day.
5. A game should not have a gameplay before the date it was added

## NOTE
- Your APIs should be fast and response messages clear.
- Add the running stats of each request. It should show how long it took and how much memory it consumed. Return it in your response header as (X-runtime & X-memory-used) or something related to that.
- You should have tests for your APIs.
- If you want to build it with a framework, use Laravel or Lumen.
- You do not need to spend so much time on documentation. A postman collection (link or JSON) will suffice.
- Push the application to Github, go to **[this link](https://rel.hm/jpx5o)**, and provide the url to the github repo.
- Write a clear README on how to deploy the application locally. Feel free to write a bash script to manage the entire setup.
- You can set up the project in a container, preferably docker.
- Deploy the API to Heroku or any other hosting platform that can allow someone access your API
- You can add authentication to the API but it is not necessary. If you add authentication, please ensure you clearly describe how to authenticate and use the API.
- Finally, include a breakdown of your thought process building the application.


> :bulb: Do not forget to have fun while doing this. It's not that serious :wink: 